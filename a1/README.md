> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331

## Andrew Wong

### Assignment 1 Requirements:

*Four parts:*

1. Distributed version control with Git and Bitbucket
2. Development installations
3. Chapter questions (Chs 1,2)
4. Bitbucket repo links: a) this assignment and b) the completed tutorials above (Bitbucketstationlocations)

#### README.md file should include the following items:

* Screenshot of running java Hello
* Screenshot of running Android Studio - My First App
* Screenshot of running Android Studio - Contacts App
* Git commands w/short descriptions

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

git init - Create an empty Git repository or reinitialize an existing one

git status -  Show the working tree status

git add - Add file contents to the index

git commit - Record changes to the repository

git push - Update remote refs along with associated objects

git pull - Fetch from and integrate with another repository or a local branch

git rm - Remove files from the working tree and from the index
 

#### Assignment Screenshots:

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/Java_Mobile_scrnshot.PNG)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/My_First_App.PNG)

*Screenshots of Android Studio - Contacts App*:

![Android Studio Contacts App Buttons](img/Contact_buttons.PNG)

![Android Studio Contacts App Info](img/Contact_info.PNG)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
