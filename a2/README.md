> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331

## Andrew Wong

### Assignment 2 Requirements:

1. Create Tip Calculator application
2. Add background color to the app
3. Create launcher icon image for the app
4. Provide screenshots of working app and skillsets

#### README.md file should include the following items:

* Screenshot of unpopulated user interface from Tip Calculator app
* Screenshot of populated user interface from Tip Calculator app
* Screenshot of skillsets 1, 2, and 3


#### Assignment Screenshots:

*Tip Calculator Unpopulated Interface*:

![Tip Calculator Unpopulated Interface Screenshot](img/TipCalculator_unpop.PNG)

*Tip Calculator Populated Interface*:

![Tip Calculator Populated Interface Screenshot](img/TipCalculator_pop.PNG)

*Skillset 1 - Circle*:

![Skillset 1 - Circle Screenshot](img/circle_scrnshot.PNG)

*Skillset 2 - MultipleNumbers*:

![Skillset 2 - MultipleNumbers Screenshot](img/multiplenumber_scrnshot.PNG)

*Skillset 3 - Exam Scores*:

![Skillset 3 - Nested Structures 2](img/nestedstructures2_scrnshot.PNG)
