> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331

## Andrew Wong

### Project 2 Requirements:

1. Create a My User app with add, update, view, and delete features
2. Add background color to the app
3. Create a launcher icon for the app
4. Provide screenshots in project 2 README file

#### README.md file should include the following items:

* Screenshot of spash screen for My Users
* Screenshot of using the add user feature
* Screenshot of using the update user feature
* Screenshot of using the view user feature
* Screenshot of using the delete user feature


#### Assignment Screenshots:

|*Spash Screen*:|*Add User*:|*Update User*:|
|---------------|-----------|--------------|
|![P2 Splash Screen Screenshot](img/p2splash.PNG)|![P2 Add User Screenshot](img/p2add.PNG)|![P2 Update User Screenshot](img/p2update.PNG)|

|*View Users*:|*Delete user*:|
|-------------|--------------|
|![P2 View Users Screenshot](img/p2view.PNG)|![P2 Delete User Screenshot](img/p2delete.PNG)|
