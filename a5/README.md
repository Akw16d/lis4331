> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331

## Andrew Wong

### Assignment 5 Requirements:

1. Create a News Reader app
2. Add background color to the app
4. Create launcher icon for the app
3. Provide screenshots of app running and skillsets (13, 14, 15)

#### README.md file should include the following items:

* Screenshot of items activity page
* Screenshot of item activity page
* Screenshot of read more link
* Screenshots of skillsets 13, 14, and 15


#### Assignment Screenshots:

|*Items Activity Page*:|*Item Activity Page*:|*Read More Page*:|
|----------------------|---------------------|-----------------|
|![A5 Items Page Screenshot](img/a5items.PNG)|![A5 Item Page Screenshot](img/a5item.PNG)|![A5 Read More Page Screenshot](img/a5more.PNG)|

*Skillset 13 - Write/Read File*:

![Skillset 13 Screenshot](img/mss13.PNG)

*Skillset 14 - Simple Interest Calculator*:

![Skillset 14 Screenshot](img/mss14.PNG)

*Skillset 15 - Array Demo Using Methods*:

![Skillset 15 Screenshot](img/mss15.PNG)
