> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331

## Andrew Wong

### Assignment 4 Requirements:

1. Create a Home Mortgage app
2. Add background color and create launcher icon for the app
4. Provide screenshots of app running and skillsets
5. Create a splash/loading screen

#### README.md file should include the following items:

* Screenshot of splash screen
* Screenshot of main page of Home Mortgage app
* Screenshot of invalid entry
* Screenshot of valid entry
* Screenshot of skillsets 10, 11, and 12


#### Assignment Screenshots:

*Splash Screen*:

![Splash Screen Screenshot](img/a4splash.PNG)

*Home Mortgage Main Page*:

![Home Mortgage Main Page Screenshot](img/a4main.PNG)

*Home Mortgage Invalid Entry Page*:

![Home Mortgage Invalid Entry Page Screenshot](img/a4invalid.PNG)

*Home Mortgage Valid Entry Page*:

![Home Mortgage Valid Entry Page Screenshot](img/a4valid.PNG)

*Skillset 10 - Travel Time*:

![Skillset 10 - Travel Time Screenshot](img/ss10.PNG)

*Skillset 11 - Product Class*:

![Skillset 11 - Product Class Screenshot](img/ss11.PNG)

*Skillset 12 - Book Inherits Product Class*:

![Skillset 12 - Book Inherits Product Class Screenshot](img/ss12.PNG)
