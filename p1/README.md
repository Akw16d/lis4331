> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331

## Andrew Wong

### Project 1 Requirements:

1. Create music player app using MediaPlayer and onClickListener
2. Complete skill sets 7, 8, and 9
3. Provide screenshots to Bitbucket

#### README.md file should include the following items:

* Screenshots of MyMusic app running
* Screenshot of skill sets 7, 8, and 9

#### Assignment Screenshots:

*Splash Screen From MyMusic app*:

![Splash Image Screenshot](img/p1splash.PNG)

*Opening Screen From MyMusic App*:

![MyMusic Opening Screenshot](img/p1open.PNG)

*Playing Screen From MyMusic App*:

![MyMusic Playing Screenshot](img/p1play.PNG)

*Pausing Screen From MyMusic App*:

![MyMusic Pausing Screenshot](img/p1pause.PNG)

*Skill Set 7*:

![Skill Set 7 Screenshot](img/ss7.PNG)

*Skill Set 8*:

![Skill Set 8 Unpopulated Interface Screenshot](img/ss8unpop.PNG)

![Skill Set 8 Populated Interface Screenshot](img/ss8pop.PNG)

*Skill Set 9*:

![Skill Set 9 Unselected Interface Screenshot](img/ss9unselect.PNG)

![Skill Set 9 Single Selection Screenshot](img/ss9single.PNG)

![Skill Set 9 Multiple Selection Screenshot](img/ss9multiple.PNG)

