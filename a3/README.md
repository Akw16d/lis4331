> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331

## Andrew Wong

### Assignment 3 Requirements:

1. Create a Currency Converter app
2. Add background color to the app
3. Create launcher icon image for the app
4. Provide screenshots of working app and skillsets
5. Create a splash/loading screen

#### README.md file should include the following items:

* Screenshot of splash screen
* Screenshot of unpopulated user interface from Currency Converter app
* Screenshot of populated user interface from Currency Converter app
* Screenshot of invalid entry, toast notification
* Screenshot of skillsets 4, 5, and 6


#### Assignment Screenshots:

*Splash Screen*:

![Splash Screen Screenshot](img/splash_scrnshot.PNG)

*Currency Converter Unpopulated Interface*:

![Currency Converter Unpopulated Interface Screenshot](img/cc_unpop_scrnshot.PNG)

*Currency Converter Populated Interface*:

![Currency Converter Populated Interface Screenshot](img/cc_pop_scrnshot.PNG)

*Currency Converter Toast Notification*:

![Currency Converter Toast Notification Screenshot](img/cc_invalid_scrnshot.PNG)

*Skillset 4 - Time Conversion*:

![Skillset 4 - Time Conversion Screenshot](img/ss_timeconversion.PNG)

*Skillset 5 - Even Or Odd (GUI)*:

![Skillset 5 - Even Or Odd (GUI) part 1 Screenshot](img/eoo1.PNG)
![Skillset 5 - Even Or Odd (GUI) part 2 Screenshot](img/eoo2.PNG)
![Skillset 5 - Even Or Odd (GUI) part 3 Screenshot](img/eoo3.PNG)
![Skillset 5 - Even Or Odd Data Validation (GUI) part 1 Screenshot](img/eoodv1.PNG)
![Skillset 5 - Even Or Odd Data Validation (GUI) part 2 Screenshot](img/eoodv2.PNG)
![Skillset 5 - Even Or Odd Data Validation (GUI) part 3 Screenshot](img/eoodv3.PNG)
![Skillset 5 - Even Or Odd Data Validation (GUI) part 4 Screenshot](img/eoodv4.PNG)

*Skillset 6 - Paint Calculator*:

![Skillset 6 - Paint Calculator part 1 Screenshot](img/calc1.PNG)
![Skillset 6 - Paint Calculator part 2 Screenshot](img/calc2.PNG)
![Skillset 6 - Paint Calculator part 3 Screenshot](img/calc3.PNG)
![Skillset 6 - Paint Calculator part 4 Screenshot](img/calc4.PNG)
![Skillset 6 - Paint Calculator part 5 Screenshot](img/calc5.PNG)
![Skillset 6 - Paint Calculator part 6 Screenshot](img/calc6.PNG)


