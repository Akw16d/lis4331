> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4331 - Advanced Mobile Applications Development

## Andrew Wong

### Assignment Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Android Studio and create My First App and Contacts App
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials 
    - Provide git command description

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create Tip Calculator application
    - Add background color to the app
    - Create launcher icon image for the app
    - Provide screenshots of working app and skillsets

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create Currency Calculator application
    - Add color to the app
    - Create launcher icon image for the app
    - Create a splash/loading screen for the app
    - Provide screenshots of working app and skillsets

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Create a Home Mortgage app
    - Add background color and create launcher icon for the app
    - Provide screenshots of app running and skillsets
    - Create a splash/loading screen

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Create a News Reader app
    - Add background color to the app
    - Create launcher icon for the app
    - Provide screenshots of app running and skillsets (13, 14, 15)

### Project Requirements:

1. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create music player app using MediaPlayer and onClickListener
    - Complete skill sets 7, 8, and 9
    - Provide screenshots to Bitbucket

2. [P2 README.md](p2/README.md "My P2 README.md file")
    - Create a My User app with add, update, view, and delete features
    - Add background color to the app
    - Create a launcher icon for the app
    - Provide screenshots in project 2 README file

